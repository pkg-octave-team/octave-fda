function x = constrain(x, loBnd, upBnd)
%% x = constrain(x, LB, UB)
% Return a copy of x where all elements fulfill LB <= x <= UB

% Last Modiefied 2018-05-31 by Juan Pablo Carbajal
  x(x < loBnd) = loBnd;
  x(x > upBnd) = upBnd;

