fda: Functional Data Analysis
=============================
Copyright (C) 2017-2018 J. O. Ramsay

Copyright (C) 2017-2018 Hadley Wickham

Copyright (C) 2017-2018 Spencer Graves

Copyright (C) 2017-2018 Giles Hooker

These functions were developed to support functional data analysis as described in 
    
    Ramsay, J. O. and Silverman, B. W. (2005) 
    Functional Data Analysis. New York: Springer. 

They were ported from earlier versions in Matlab and S-PLUS.
An introduction appears in 

    Ramsay, J. O., Hooker, Giles, and Graves, Spencer (2009) 
    Functional Data Analysis with R and Matlab (Springer). 

The package includes data sets and script files working many examples 
including all but one of the 76 figures in this latter book.
